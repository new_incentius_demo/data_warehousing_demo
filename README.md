# Data Warehousing (data_warehousing_demo)

To run the dashboard on your localhost, follow the below steps:-

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

## Data used in this dashboard :-
Tab Name              | Json File Name
--------------------- | -------------
batch.vue             | assets/fileMaster.json,assets/Completed.json,assets/Failed.json
buckets.vue           | assets/dailyUpdate.json,assets/buckets.json
businessValid.vue     | assets/dailyUpdate.json,assets/ease_hold.json,assets/sha_hold.json,assets/sd_hold.json,assets/sp_hold.json,assets/ins_hold.json,assets/va_hold.json
businessValidArchive.vue | assets/dailyUpdate.json,assets/ease_hold.json,assets/sha_hold.json,assets/sd_hold.json,assets/sp_hold.json,assets/ins_hold.json,assets/va_hold.json
fileMaster.vue           | assets/fileMaster.json
inputTracking.vue        | assets/dailyUpdate.json,assets/tracking.json
mdm.vue                  | assets/dailyUpdate.json,assets/contentType.json
mdmRequest.vue           | assets/changeReq.json
mdmRequestSub.vue        | assets/changeReq.json
mdmRev.vue               | assets/contentType.json

The data for the different tabs are used according to different json files as mentioned above and the data for the rest of the tabs which are not mentioned above are hardcoded.
