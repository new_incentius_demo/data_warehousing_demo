module.exports = {
    theme: {
        extend: {
            height:{
                13:'3.15rem',
                72:'18rem',
                80:'20rem',
                88:'22rem',
                96:'24rem',
                98:'28.5rem',
                70:'370px',
                100:'33.5rem',
                99:'410px'
            },
            width:{
                72:'19rem',
                80:'20rem',
                88:'21rem',
                96:'24rem',
                100:'34rem',
                98:'32.5rem'
            },
            margin:{
                44:'12rem',
                72:'38.2rem'
            },
            fontSize:{
                'xxs':'0.60rem',
            }
        },
        screens:{
            'xs': '280px',
            'sm': '375px',
            'md': '768px',
            'lg':'1024px',
            'xl':'1241px'
      
        }
    },
    
    prefix:  'tw-',
    
    variants: {},
    plugins: []
}
