import Vue from 'vue'
import Vuex from 'vuex'

// import example from './module-example'
import state from './module-example/state'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 *
 */





export default function (/* { ssrContext } */) {
  
  const Store = new Vuex.Store({
    modules: {
      // example
    },
    state:{
      spData:[],
          sdData:[],
          vaData:[],
          easeData:[],
          insData:[],
      sdDataArchive:[],
      spDataArchive:[],
      insDataArchive:[],
      vaDataArchive:[],
      shaDataArchive:[],
      shaData:[],
      easeDataArchive:[],
    },
    actions:{
    },
    mutations:{
      changeBatchId(state,id){
        state.batch_id=id
      },
      archiveShaData(state,archData){
        state.shaDataArchive=archData
      },
      archiveSpData(state,archData){
        state.spDataArchive=archData
      },
      archiveSdData(state,archData){
        state.sdDataArchive=archData
      },
      archiveVaData(state,archData){
        state.vaDataArchive=archData
      },
      archiveEaseData(state,archData){
        state.easeDataArchive=archData
      },
      archiveInsData(state,archData){
        state.insDataArchive=archData
      },
      shaData(state,sha){
        state.shaData=sha
      },
      sdData(state,sd){
        state.sdData=sd
      },
      spData(state,sp){
        state.spData=sp
      },
      easeData(state,ease){
        state.easeData=ease
      },
      vaData(state,va){
        state.vaData=va
      },
      insData(state,ins){
        state.insData=ins
      },
    },
    getters:{
    },
    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEBUGGING
  })

  return Store
}
