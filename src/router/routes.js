
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/inputtracking', component: () => import('pages/inputTracking.vue') },
      { path: '/buckets', component: () => import('pages/buckets.vue') },
      { path: '/business/hold', component: () => import('pages/businessValid.vue') },
      { path: '/business/hold/archive', component: () => import('pages/businessValidArchive.vue') },
      { path: '/filemaster', component: () => import('pages/fileMaster.vue') },
      { path: '/filemaster/batch:id',name:'batch', component: () => import('pages/batch.vue') },
      { path: '/mdm', component: () => import('pages/mdm.vue') },
      { path: '/mdm/rev:id',name:'review', component: () => import('pages/mdmRev.vue') },
      { path: '/mdmrequest',name:'request', component: () => import('pages/mdmRequest.vue') },
      { path: '/mdmrequest/sub:id',name:'requestsub', component: () => import('pages/mdmRequestSub.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
